// ac自动机

let AhoCorasick;
(function () {
    AhoCorasick = function (keywords) {
        this._buildTables(keywords);
    };

    AhoCorasick.prototype._buildTables = function (keywords) {
        let gotoFn = {
            0: {}
        };
        let output = {};

        let state = 0;
        keywords.forEach(function (word) {
            let curr = 0;
            for (let i = 0; i < word.length; i++) {
                let l = word[i];
                if (gotoFn[curr] && l in gotoFn[curr]) {
                    curr = gotoFn[curr][l];
                } else {
                    state++;
                    gotoFn[curr][l] = state;
                    gotoFn[state] = {};
                    curr = state;
                    output[state] = [];
                }
            }

            output[curr].push(word);
        });

        let failure = {};
        let xs: any = [];

        // f(s) = 0 for all states of depth 1 (the ones from which the 0 state can transition to)
        for (let l  of Object.keys(gotoFn[0])) {
            let state = gotoFn[0][l];
            failure[state] = 0;
            xs.push(state);
        }

        while (xs.length) {
            let r = xs.shift();
            // for each symbol a such that g(r, a) = s
            // = 0; l < gotoFn[r!].length; l++
            for (let l  of Object.keys(gotoFn[r])) {
                let s = gotoFn[r!][l];
                xs.push(s);

                // set state = f(r)
                let state = failure[r!];
                while (state > 0 && !(l in gotoFn[state])) {
                    state = failure[state];
                }

                if (l in gotoFn[state]) {
                    let fs = gotoFn[state][l];
                    failure[s] = fs;
                    output[s] = output[s].concat(output[fs]);
                } else {
                    failure[s] = 0;
                }
            }
        }

        this.gotoFn = gotoFn;
        this.output = output;
        this.failure = failure;
    };

    AhoCorasick.prototype.search = function (string) {
        let state = 0;
        let results: any = [];
        for (let i = 0; i < string.length; i++) {
            let l = string[i];
            while (state > 0 && !(l in this.gotoFn[state])) {
                state = this.failure[state];
            }

            if (!(l in this.gotoFn[state])) {
                continue;
            }

            state = this.gotoFn[state][l];

            if (this.output[state].length) {
                let foundStrs = this.output[state];
                results.push([i, foundStrs]);
            }
        }

        return results;
    };

    // if (typeof module !== 'undefined') {
    //     module.exports = AhoCorasick;
    // } else {
    //     AhoCorasick = AhoCorasick;
    // }
})();

class RealNumber {
    public regTxt: string; // 第%x%章
    public decimalLvl: string; // 第几项
    public realTxt: string;  // 显示标号
    public txt: string;   // P txt
    public father: RealNumber | Root;  // 父节点
    public roots: any[]; // 子节点
    public type = 0;  // 1数字 2简体汉字 3繁体汉字 4小写字母 5大写字母
    public range: {
        start: { line: number|null, character: number|null },
        end: { line: number|null, character: number|null }
    };

    constructor() {
        this.range = {
            start: {line: null, character: null},
            end: {line: null, character: null}
        };
        this.roots = [];
    }

    toString() {
        return "regTxt:" + this.regTxt + "  decimalLvl:" + this.decimalLvl + " realTxt:" + this.realTxt + " type:" + this.type;
    }
}

class Root {
    public roots: any[];
    public name = null;
    public uri = null;

    constructor() {
        this.roots = [];
    }
}

class ReturnNode {
    public regTxt: string|null; // 第%x%章
    public decimalLvl: string|null; // 第几项
    public txt: string|null;   // P txt
    public type = 0;  // 1数字 2简体汉字 3繁体汉字 4小写字母 5大写字母 0是错误的、无法解析的
    public name = null;
    public detail = null;
    public kind = 14;
    public range: {
        "start": { "line": number|null, "character": number|null },
        "end": { "line": number|null, "character": number|null }
    };
    public selectionRange: {
        "start": { "line": number|null, "character": number|null },
        "end": { "line": number|null, "character": number|null }
    };
    public children: any[];

    constructor() {
        this.range = {
            start: {line: null, character: null},
            end: {line: null, character: null}
        };
        this.selectionRange = {
            start: {line: null, character: null},
            end: {line: null, character: null}
        };
        this.children = [];
    }
}

export class Content {
    public rootNode: null|Root  = null; // 文章根节点
    public nodesLocation = {}; // 所有node的位置
    public linksLocation = {}; // 所有引用node的link的位置
    public nodesJson: string|null = null; // 所有node 返回Json
    Simplified_Chinese_character  = "零一二三四五六七八九十百千万亿";
    Traditional_Chinese_character = "〇壹贰叁肆伍陆柒捌玖拾佰仟万亿";
    Arabic_character = "0123456789";
    Uppercase_letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    Lowercase_letter = "abcdefghijklmnopqrstuvwxyz";
    upperPattern = /^第?([\d零一二三四五六七八九十百千〇壹贰叁肆伍陆柒捌玖拾佰仟万亿\.]+)[条章项]/;
    decimalPattern = /^(\(?[a-zA-Z\d\.]+\)?[、;；:：]?)/;

    /**
     * chieseCharacter2Arabic
     * @param str
     */
    chineseToNumber(str) {
        if (str.includes('千')) {
            const index = str.lastIndexOf('千');
            if (index === 0) {
                return 1000 + this.chineseToNumber(str.substring(index + 1));
            } else if (index === str.length - 1) {
                return this.chineseToNumber(str.substring(0, index)) * 1000
            } else {
                return this.chineseToNumber(str.substring(0, index)) * 1000 + this.chineseToNumber(str.substring(index + 1))
            }
        } else if (str.includes('百')) {
            const index = str.lastIndexOf('百');
            if (index === 0) {
                return 100 + this.chineseToNumber(str.substring(index + 1));
            } else if (index === str.length - 1) {
                return this.chineseToNumber(str.substring(0, index)) * 100
            } else {
                return this.chineseToNumber(str.substring(0, index)) * 100 + this.chineseToNumber(str.substring(index + 1))
            }
        } else if (str.includes('十')) {
            const index = str.lastIndexOf('十');
            if (index === 0) {
                return 10 + this.chineseToNumber(str.substring(index + 1));
            } else if (index === str.length - 1) {
                return this.chineseToNumber(str.substring(0, index)) * 10
            } else {
                return this.chineseToNumber(str.substring(0, index)) * 10 + this.chineseToNumber(str.substring(index + 1))
            }
        } else if (str.includes('点')) {
            const index = str.lastIndexOf('点');
            if (index === 0) {
                return Number('.' + this.chineseToNumber(str.substring(1)));
            } else if (index === str.length - 1) {
                return this.chineseToNumber(str.substring(0, index))
            } else {
                return Number(this.chineseToNumber(str.substring(0, index)) + '.' +  this.chineseToNumber(str.substring(index + 1)));
            }
        } else {
            const numberArr = [
                {
                    chinese: '九',
                    number: 9
                },
                {
                    chinese: '八',
                    number: 8
                },
                {
                    chinese: '七',
                    number: 7
                },
                {
                    chinese: '六',
                    number: 6
                },
                {
                    chinese: '五',
                    number: 5
                },
                {
                    chinese: '四',
                    number: 4
                },
                {
                    chinese: '三',
                    number: 3
                },
                {
                    chinese: '二',
                    number: 2
                },
                {
                    chinese: '一',
                    number: 1
                }
            ]
            let number = -1;
            for (let i = 0; i < numberArr.length; i++) {
                let numberObj = numberArr[i];
                if (str.includes(numberObj['chinese'])) {
                    number = numberObj['number'];
                }
            }
            if (number > -1) {
                return number;
            } else {
                return 0;
            }
        }
    }
    /**
     * 加载文章树形结构
     * @param pList 文章数组
     * @returns {Root}
     */
    getArticleTree(pList) {
        let serialResult: any = [];
        for (let i = 0; i < pList.length; i++) {
            let num = pList[i];
            if (num.match(this.upperPattern)) {
                let realNumber = new RealNumber();
                realNumber.txt = num;
                realNumber.decimalLvl = num.match(this.upperPattern)[1];
                realNumber.realTxt = num.match(this.upperPattern)[0];
                if (num.match(this.upperPattern)[1].split(".").length > 1) {
                    let pattern = /([\d一二三四五六七八九零〇]+$)/;
                    if (realNumber.decimalLvl.match(pattern)) {
                        realNumber.decimalLvl = realNumber.decimalLvl.match(pattern)![1] ;
                    }
                }

                let index = num.match(this.upperPattern)[0].lastIndexOf(realNumber.decimalLvl);
                let strStart = num.match(this.upperPattern)[0].substring(0, index);
                let strEnd = num.match(this.upperPattern)[0].substring(index + realNumber.decimalLvl.length);

                // 需要判断decimalLvl的内容 更改type 然后将decimalLvl改成相应真实值
                let indexCharacter = 1;
                for (let macthCharacter of [this.Arabic_character, this.Simplified_Chinese_character, this.Traditional_Chinese_character]) {
                    if (realNumber.type !== 0) {
                        break;
                    }
                    for (let char of realNumber.decimalLvl) {
                        if (macthCharacter.indexOf(char) === -1) {
                            realNumber.type = 0;
                            break;
                        }
                        realNumber.type = indexCharacter;
                    }
                    indexCharacter += 1;
                }
                switch (realNumber.type) {
                    case 1:
                        // 数字不会变
                        // realNumber.decimalLvl = "1";
                        break;
                    case 2:
                        // 简中
                        realNumber.decimalLvl = this.chineseToNumber(realNumber.decimalLvl);
                        break;
                    case 3:
                        for (let charN = 0; charN < this.Traditional_Chinese_character.length; charN++) {
                            realNumber.decimalLvl = realNumber.decimalLvl.replace(this.Traditional_Chinese_character[charN], this.Simplified_Chinese_character[charN]);
                        }
                        break;
                }
                realNumber.regTxt = strStart + "%x%" + strEnd;
                realNumber.range.start.line = i;
                realNumber.range.start.character = 0;
                realNumber.range.end.line = i;
                realNumber.range.end.character = realNumber.range.start.character + realNumber.realTxt.length;
                serialResult.push(realNumber);
            } else if (num.match(this.decimalPattern)) {
                let realNumber = new RealNumber();
                realNumber.txt = num;
                realNumber.decimalLvl = num.match(this.decimalPattern)[1];
                realNumber.realTxt = num.match(this.decimalPattern)[0];
                if (realNumber.decimalLvl.split(".").length > 1) {
                    let pattern = /([\d一二三四五六七八九零〇]+$)/;
                    if (realNumber.decimalLvl.match(pattern)) {
                        realNumber.decimalLvl = realNumber.decimalLvl.match(pattern)![1]
                    }
                }
                let index = num.match(this.decimalPattern)[0].lastIndexOf(realNumber.decimalLvl);
                let strStart = num.match(this.decimalPattern)[0].substring(0, index);
                let strEnd = num.match(this.decimalPattern)[0].substring(index + realNumber.decimalLvl.length);

                // 需要判断decimalLvl的内容 更改type 然后将decimalLvl改成相应真实值
                let indexCharacter = 1;
                for (let macthCharacter of [this.Arabic_character, this.Simplified_Chinese_character,
                    this.Traditional_Chinese_character, this.Lowercase_letter, this.Uppercase_letter]) {
                    if (realNumber.type !== 0) {
                        break;
                    }
                    for (let char of realNumber.decimalLvl) {
                        if (macthCharacter.indexOf(char) === -1) {
                            realNumber.type = 0;
                            break;
                        }
                        realNumber.type = indexCharacter;
                    }
                    indexCharacter += 1;
                }
                switch (realNumber.type) {
                    case 1:
                        // 数字不会变
                        // realNumber.decimalLvl = "1";
                        break;
                    case 2:
                        // 简中
                        realNumber.decimalLvl = this.chineseToNumber(realNumber.decimalLvl);
                        break;
                    case 3:
                        for (let charN = 0; charN < this.Traditional_Chinese_character.length; charN++) {
                            realNumber.decimalLvl = realNumber.decimalLvl.replace(this.Traditional_Chinese_character[charN], this.Simplified_Chinese_character[charN]);
                        }
                        break;
                    case 4:
                        // 小写字母
                        break;
                    case 5:
                        // 大写字母
                        break;
                }
                realNumber.regTxt = strStart + "%x%" + strEnd;
                realNumber.type = 1;
                realNumber.range.start.line = i;
                realNumber.range.start.character = 0;
                realNumber.range.end.line = i;
                realNumber.range.end.character = realNumber.range.start.character + realNumber.realTxt.length;
                serialResult.push(realNumber);
            } else {
                let realNumber = new RealNumber();
                realNumber.txt = num;
                serialResult.push(realNumber);
            }
        }

        let serialMap = {};
        let Father = [];
        let rootNode = new Root();
        let isLastOne: RealNumber | null = null;

        for (let i = 0; i < serialResult.length; i++) {
            let one = serialResult[i];

            if (isLastOne == null) {
                // 前面的都不带编号
                rootNode.roots.push(one);
                one.father = rootNode;
                if (one.regTxt != null) {
                    isLastOne = one;
                    serialMap[one.regTxt] = one;
                }
                continue;
            }

            if (one.regTxt == null) {
                // 说明不带编号
                one.father = isLastOne.father;
                one.father.roots.push(one);
                continue;
            }

            if (serialMap[one.regTxt] == null) {
                // serialMap中还未有
                one.father = isLastOne;
                one.father.roots.push(one);
                serialMap[one.regTxt] = one;
                isLastOne = one;
            } else {
                let other = serialMap[one.regTxt];
                one.father = other.father;
                one.father.roots.push(one);
                isLastOne = one;

                let start = false;
                for (let i of Object.keys(serialMap)) {
                    if (start) {
                        serialMap[i] = undefined;
                    }
                    if (i === one.regTxt) {
                        start = true;
                    }
                }
            }
        }
        return rootNode;
    }

    getArticleTreeNodes(node) {
        let returnJson: any = [];
        if (node != null) {
            for (let i = 0; i < node.roots.length; i++) {
                let thisNode = node.roots[i];

                if (thisNode.regTxt == null) {
                    continue;
                }
                let r = new ReturnNode();
                r.name = thisNode.realTxt;
                r.regTxt = thisNode.regTxt;
                r.decimalLvl = thisNode.decimalLvl;
                r.txt = thisNode.txt;
                r.type = thisNode.type;
                r.selectionRange["start"]["line"] = thisNode.range.start.line;
                r.selectionRange["start"]["character"] = thisNode.range.start.character;
                r.selectionRange["end"]["line"] = thisNode.range.end.line;
                r.selectionRange["end"]["character"] = thisNode.range.end.character;
                r.range["start"]["line"] = 0;
                r.range["start"]["character"] = thisNode.range.start.character;
                r.range["end"]["line"] = thisNode.range.end.line;
                r.range["end"]["character"] = thisNode.txt.length;
                // r.containerName = thisNode.father.realTxt;
                returnJson.push(r);
                this.nodesLocation[thisNode.realTxt] = r.selectionRange;
                // 更改结构
                r.children = this.getArticleTreeNodes(thisNode);
                // returnJson = returnJson.concat(getArticleTreeNodes(thisNode));
            }
        }
        return returnJson;
    }

    getLinkLocation(file: any[]) {
        let keys: any = [];
        for (let key of Object.keys(this.nodesLocation)) {
            keys.push(key);
        }
        let ac = new AhoCorasick(keys);
        for (let line = 0; line < file.length; line++) {
            let searchResults = ac.search(file[line]);
            let lastIndex = Number.MAX_VALUE; // 重合消除
            for (let index = searchResults.length - 1; index >= 0; index--) {
                let lineResults = searchResults[index];
                if (lineResults[0] > lastIndex) {
                    continue;
                }
                // 相同结尾 保留最长的String 也就是第一个
                lineResults[1] = lineResults[1][0];
                if (lineResults[1].length - 1 < searchResults[index][0]) {
                    let startIndex = (lineResults[0] - lineResults[1].length + 1);
                    // console.log("line:"+ line + " index:" + startIndex + "-"+lineResults[0]+"  txt:" + lineResults[1]);
                    if (this.linksLocation[line] == null) {
                        this.linksLocation[line] = []
                    }
                    this.linksLocation[line].push([startIndex, lineResults[0], lineResults[1]]);
                    lastIndex = lineResults[0] - lineResults[1].length;
                } else {
                    // 说明就是开头序号
                }
            }
        }
        // console.log(linksLocation);
    }

    /**
     * 通过在文章中的位置，返回其link的引用位置
     * @param json
     * @returns {null|string} 如果没有link 则为null
     */
    getNodeByLocation(fileString: string, json: string) {
        this.parseContent(fileString);

        let position = JSON.parse(json);
        let line = position["position"]["line"];
        let character = position["position"]["character"];

        // console.log(line +"|"+character);
        if (this.linksLocation[line] == null) {
            return null;
        } else {
            for (let linkIndex of Object.keys(this.linksLocation[line])) {
                let link = this.linksLocation[line][linkIndex];
                if (link[0] <= character && link[1] >= character) {
                    let returnJson = [{
                        "uri": position["textDocument"]["uri"],
                        "range": this.nodesLocation[link[2]]
                    }];

                    return JSON.stringify(returnJson);
                }
            }
        }
        return null;
    }
    /**
     * 通过解析file来初始化
     * @param fileString 需要文章原文、
     * @returns {string} json
     */
    parseContent(fileString: string ): string {
        this.rootNode = null; // 文章根节点
        this.nodesLocation = {}; // 所有node的位置
        this.linksLocation = {}; // 所有引用node的link的位置
        this.nodesJson = null; // 所有node 返回Json
        let file = fileString.split("\n");
        this.rootNode = this.getArticleTree(file);
        this.nodesJson = JSON.stringify(this.getArticleTreeNodes(this.rootNode));
        this.getLinkLocation(file);
        return this.nodesJson;
    }
    constructor() {
        this.rootNode = null; // 文章根节点
        this.nodesLocation = {}; // 所有node的位置
        this.linksLocation = {}; // 所有引用node的link的位置
        this.nodesJson = null; // 所有node 返回Json
    }
}
let content = new Content();

let fileString = "集团采购框架合同\n" +
    "甲方(采购方):××××有限公司\n" +
    "乙方(供应方):\n" +
    "第一条 用语和定义\n" +
    "1.1 产品\n" +
    "  亦称“采购产品”，是指甲方向乙方采购产品的总称，具体类型包括但不限于附件1《采购清单》列明的内容，采购产品详细信息以甲方通过供应商管理电子系统(即甲方的ERP管理系统或甲方认可的其他电子采购管理系统)向乙方出具的《采购清单》及相关系统数据为准。\n" +
    "\n" +
    "1.2 合同与其他法律文件\n" +
    "本合同用于调整双方间法律关系，其他法律文件用于具体解释本合 同和促使具体交易顺利履行，本合同项下其他法律文件" +
    "包括但不限于: 履约过程中就具体问题的书面约定的合同、协议等文件;质量保证协议 书;采购清单;供应商调查表、承认书、" +
    "报价单、包装要求、货物进口委托书;因便利合作需要，乙方不提供经盖章同意的采购清单、承认书、供应商调查表、报价单、包装要求、质量标准补充要求等法律文件的原 件，乙方通过供应商管理电子系统明确同意的文件、数据、资料以及通过本合同指定电子邮箱发送的文件内容均为本合同项下交易的有效法 律凭据。\n" +
    "\n" +
    "1.3 关联企业\n" +
    "本合同关联企业系指甲方、甲方的母公司、子公司或控股公司等其 他关联公司。\n" +
    "\n" +
    "1.4 采购清单\n" +
    "系指甲方或其关联企业通过书面形式向乙方下达的购买产品的文 件。除非特别声明，否则双方间发生的所有采购清单均视为本合同项下的采购清单。甲方有权对乙方资质和履约能力予以稽核后就具体拟采购 产品出具采购清单，产品信息以采购清单等相关文件的内容为准。除非以书面形式特别声明，否则其他法律文件不因签订在后而更改本合同所 载权利义务。\n" +
    "\n" +
    "1.5 书面形式\n" +
    "是指文书、信件、数据电文(包括电报、电传、传真、电子数据交 换和电子邮件)、电子系统等可以有形地表现所载内容的形式。\n" +
    " \n" +
    "第二条 双方关系\n" +
    "2.1 合同目的\n" +
    "  甲乙双方签订本合同的目的，系保证甲方得以在世界范围内将采购清单所述产品用于加工成甲方电子产品并进行销售;" +
    "直接将采购清单所述产品作为甲方产品销售给第三方(无论是否标识乙方为生产者);甲方在世界范围内维护其良好的商业信誉;乙方得以在完整履行本合同所有义务的前提下收取价款。甲乙各方充分理解前述合同目的，并同意对彼此利益给予充分、及时的维护和补偿。\n" +
    "2.2 交易模式\n" +
    "  除非另有约定，否则甲方在通知乙方后可由关联企业行使合同权利，或通过关联企业履行合同义务，前述权利的行使和义务的履行均为合法;或关联企业可依据本合同向乙方下达采购清单等相关文件，该相关文件对乙方具有约束力。但未经另行约定，甲方及其关联企业就各自采购清单互不承担连带责任，乙方仅得向采购清单下达方主张权利。\n" +
    "2.3 合同期限\n" +
    "  本合同自甲乙双方签字和盖条之日起生效，有效期暂定一年，或至甲方依据本合同约定通知解除本合同之日止。但本合同" +
    "被解除或终止，不影响已经执行当中的采购清单继续履行。同时也不影响乙方对于订单货物的质保义务，下单方与乙方的权利义务仍然按照本合同约定执行。根据2.2条规定的交易模式，甲方可自己或由任何其他供应商(承 包商)完成本合同项目，甲方有权要求乙方支付完成本合同项目所 招致的所有增加的费用。\n" +
    "  ";
let positionJson = "{\"textDocument\":{\"uri\":\"filpl.java\"},\"position\":{\"line\":21,\"character\":46}}\n";
console.log(content.parseContent(fileString));
console.log(content.getNodeByLocation(fileString, positionJson));